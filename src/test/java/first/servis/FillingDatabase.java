package first.servis;

import first.model.Database;
import first.model.Person;
import util.InputUtil;

public class FillingDatabase {
    Database database = new Database();

    public void run() {
        database.printOption();
        int starting = InputUtil.nextInt("Choose an action");
        while (starting != 0) {
            switch (starting) {
                case 1:
                    database.add(Person.createDataPerson());
                    break;
                case 2:
                    String name = InputUtil.nextString("Enter name");
                    int age = InputUtil.nextInt("Enter age");
                    database.delete(name,age);
                    break;
                case 3:
                    database.printAll();
                    break;
                default:
                    throw new RuntimeException();
            }
            database.printOption();
            starting = InputUtil.nextInt("Choose an action");
        }

    }
}
