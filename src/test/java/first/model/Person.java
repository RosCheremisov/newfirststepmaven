package first.model;

import lombok.Data;
import util.InputUtil;

public @Data class Person {
    String name;
    int age;

    public static Person createDataPerson(){
        Person person = new Person();
        person.name = InputUtil.nextString("Enter name");
        person.age = InputUtil.nextInt("Enter age");
        return person;

    }
}
