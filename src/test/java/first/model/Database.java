package first.model;


import java.util.HashSet;
import java.util.Set;

public class Database {
    private final Set<Person> personDatabase = new HashSet<Person>();

    public void add(Person person){
        if(personDatabase.contains(person)){
            System.out.println("Such a person already exists");
        } else {
            personDatabase.add(person);
            System.out.println("Person " + person.name + " Age: " + person.age + " - added");
        }
    }

    public void delete(String name,int age){
        Person person = new Person();
        person.name = name;
        person.age = age;
        if(personDatabase.contains(person)){
            personDatabase.remove(person);
            System.out.println("Person " + person.name + " Age: " + person.age + " - deleted");
        }else{
            System.out.println("Which person is not in the database");
        }
    }

    public void printAll(){
        for(Person person: personDatabase){
            System.out.println(person);
        }
    }

    public void printOption(){
        System.out.println("1-Add");
        System.out.println("2-Delete");
        System.out.println("3-Print List");
    }

}

