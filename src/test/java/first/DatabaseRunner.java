package first;

import first.servis.FillingDatabase;

public class DatabaseRunner {
    public static void main(String[] args) {
        FillingDatabase database = new FillingDatabase();
        database.run();
    }
}
